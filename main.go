// back project main.go
package main

import (
	"fmt"
	"log"
	"math"
	"os"
)

var startCapital float64 = 100
var days_per_year float64 = 3650

var rateFunc = trueRate;

// the function from lecture example
func trueRate(time float64) float64 {
	return (0.04 * (3 - 2*float64(math.Cos(float64(2*time))) + time/3.0))
}

func homeRate(time float64) float64{
	return (0.08 + 0.01*time*(7-time) )
}

func buildReal(fn func(float64) float64, period int) []float64 {
	dataSet := make([]float64, int(period*int(days_per_year)))

	for i := range dataSet {
		dataSet[i] = fn(float64(float64(i) / days_per_year))
	}

	return dataSet
}

// the smooth sets inttegration time stamp
func buildInfoSet(fn func(float64) float64, period int, smooth float64) []float64 {
	dataSet := make([]float64, int(period*int(days_per_year)))

	integrate := func(time float64) float64 {
		var sum float64 = 0
		for t := 0.0; t < time; t += smooth {
			inter := fn(t)
			sum += float64(inter * smooth)
		}
		return sum
	}

	for i := range dataSet {
		dataSet[i] = float64(int(100*float64(startCapital*math.Exp(integrate(float64(float64(i)/days_per_year)))))) / 100
	}

	return dataSet
}

//timeStap in days
func buildRestored(data []float64, timeStap float64, smooth float64) []float64 {
	restoredSet := make([]float64, len(data))

	dt := timeStap * days_per_year

	for t := 0.0; t < float64(len(data))-dt; t += dt {
		restoredSet[int(t+dt/2)] = (days_per_year) * (data[int(t+dt)] - data[int(t)]) / (data[int(t)] * dt)

		// interpolate linear
		if t > 0 {
			cntr:=0.0;
			for i := int(t + dt/2 - dt); i < int(t+dt/2)-1; i++ {
				cntr++
				restoredSet[i+1] = restoredSet[int(t + dt/2 - dt)] + ((restoredSet[int(t+dt/2)]-restoredSet[int(t + dt/2 - dt)])/(dt))*cntr
			}
		}

	}

	return restoredSet
}

func main() {
	fmt.Println("More interesting task solver!")
	fo, err := os.Create("back.txt")
	if err != nil {
		log.Fatal(err)
	}

	//	timestamp := 10
	init_interest := buildReal(rateFunc, 5)
	init_capital := buildInfoSet(rateFunc, 5, 0.0001)
	restored := buildRestored(init_capital, 1/days_per_year, 0.0001)

	var rms float64= 0;
	for i := range init_interest {
		fo.WriteString(fmt.Sprintf("%f\t%f\t%f\n", init_capital[i], init_interest[i], restored[i]))
		rms += (init_interest[i] - restored[i])*(init_interest[i] - restored[i])
	}
	rms = math.Sqrt(rms)
	fmt.Printf("Root mean square deviation = %f\n",rms);

	fo.Close()

}
